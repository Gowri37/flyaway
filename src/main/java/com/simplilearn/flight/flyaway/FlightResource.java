package com.simplilearn.flight.flyaway;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.simplilearn.flight.flyaway.entity.Flight;

import com.simplilearn.flight.flyaway.entity.dao.FlightDao;

@Path("/flights")
public class FlightResource {

	@GET
	@Produces("application/json")
	public List<Flight> getFlight() {
		FlightDao dao = new FlightDao();
		List<Flight> flights = dao.getFlights();
		return flights;
	}

	@Path("/create")
	@POST
	@Consumes("application/json")
	public Response addFlight(Flight flight) {

		FlightDao dao = new FlightDao();
		dao.addFlight(flight);

		return Response.ok().build();
	}

	@PUT
	@Path("/{id}")
	@Consumes("application/json")
	public Response updateFlight(@PathParam("id") int id, Flight flight) {
		FlightDao dao = new FlightDao();
		int count = dao.updateFlight(id, flight);
		if (count == 0) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	@Consumes("application/json")
	public Response deleteFlight(@PathParam("id") int id) {
		FlightDao dao = new FlightDao();
		int count = dao.deleteFlight(id);
		if (count == 0) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok().build();
	}
}
