package com.simplilearn.flight.flyaway;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import com.simplilearn.flight.flyaway.entity.Passenger;
import com.simplilearn.flight.flyaway.entity.dao.PassengerDao;

@Path("/user")
public class PassengerResource
{
	@GET
    @Produces("application/json")
    public List<Passenger> getUser() {
        PassengerDao dao = new PassengerDao();
        List users = dao.getUsers();
        return users;
    }
 
	@Path("/create")
    @POST
    @Consumes("application/json")
    public Response addUser(Passenger user){
                       
        PassengerDao dao = new PassengerDao();
        dao.addUser(user);
        
        return Response.ok().build();
    }
    
    @PUT
    @Path("/{id}")
    @Consumes("application/json")
    public Response updateAirport(@PathParam("id") int id, Passenger user){
        PassengerDao dao = new PassengerDao();
        int count = dao.updateUser(id, user);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
    
    @DELETE
    @Path("/{id}")
    @Consumes("application/json")
    public Response deleteAirport(@PathParam("id") int id){
        PassengerDao dao = new PassengerDao();
        int count = dao.deleteUser(id);
        if(count==0)
        {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
}

