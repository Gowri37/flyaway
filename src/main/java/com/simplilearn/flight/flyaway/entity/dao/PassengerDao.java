package com.simplilearn.flight.flyaway.entity.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.simplilearn.flight.flyaway.entity.Passenger;
import com.simplilearn.flight.flyaway.entity.util.SessionUtil;

@SuppressWarnings("deprecation")
public class PassengerDao 
{
 
	public void addUser(Passenger bean) 
	{
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		addUser(session, bean);
		tx.commit();
		session.close();

	}

	private void addUser(Session session, Passenger bean) {
		
		Passenger user= new Passenger();
		user.setFirstName(bean.getFirstName());
		user.setLastName(bean.getLastName());
		user.setEmail(bean.getEmail());
		user.setId(bean.getId());
		session.save(user);
		
	}

	
	public List<Passenger> getUsers()
	{
		Session session = SessionUtil.getSession();
		Query query = session.createQuery("from Passenger");
		List<Passenger> users = query.list();
		session.close();
		return users;
	}

	public int deleteUser(int id)
	{
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "delete from Passenger where id = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", id);
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		tx.commit();
		session.close();
		return rowCount;
	}

	public int updateUser(int id, Passenger user) 
	{
		if (id <= 0)
			return 0;
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "update Passenger set firstName = :firstName, lastName=:lastName, email=:email where id = :id";
		Query query = session.createQuery(hql);

		query.setInteger("id", id);
		query.setString("firstName", user.getFirstName());
		query.setString("lastName", user.getLastName());
		query.setString("email", user.getEmail());
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		tx.commit();
		session.close();
		return rowCount;
	}

	
}
