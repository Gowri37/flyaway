package com.simplilearn.flight.flyaway.entity.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.simplilearn.flight.flyaway.entity.Flight;
import com.simplilearn.flight.flyaway.entity.util.SessionUtil;

public class FlightDao {
	
	public void addFlight(Flight bean)
	{
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		addFlight(session, bean);
		tx.commit();
		session.close();

	}

	private void addFlight(Session session, Flight bean)
	{
		Flight flight = new Flight();

		flight.setId(bean.getId());
		flight.setDeparture(bean.getDeparture());
		flight.setArrival(bean.getArrival());
		flight.setArrivalDate(bean.getArrivalDate());
		flight.setDepartureDate(bean.getDepartureDate());
		flight.setBookings(bean.getBookings());

		session.save(flight);
	}

	public List<Flight> getFlights()
	{
		Session session = SessionUtil.getSession();
		Query query = session.createQuery("from Flight");
		List<Flight> flights = query.list();
		session.close();
		return flights;
	}
	public int updateFlight(int id, Flight flight) 
	{
		if (id <= 0)
			return 0;
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "update Flight set Departure = :Departure, arrival=:arrival, departureDate:=departureDate, arrivalDate=:arrivalDate where id = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", id);
		query.setString("Departure", flight.getDeparture());
		query.setString("Arrival", flight.getArrival());
		query.setString("ArrivalDate", flight.getArrivalDate());
		query.setString("departureDate", flight.getDepartureDate());
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		tx.commit();
		session.close();
		return rowCount;
	}
	public int deleteFlight(int id)
	{
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "delete from Flight where id = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", id);
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		tx.commit();
		session.close();
		return rowCount;
	}

}
