package com.simplilearn.flight.flyaway.entity.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.simplilearn.flight.flyaway.entity.Flight;
import com.simplilearn.flight.flyaway.entity.FlightBooking;
import com.simplilearn.flight.flyaway.entity.util.SessionUtil;

public class FlightBookingDao 
{

	public void addReservation(FlightBooking bean)
	{
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		addReservation(session, bean);
		tx.commit();
		session.close();

	}

	private void addReservation(Session session, FlightBooking bean) 
	{
		
		FlightBooking FB = new FlightBooking();
		FB.setPassenger(bean.getPassenger());
		FB.setId(bean.getId());
		FB.setFlights(bean.getFlights());
		session.save(FB);
	}

	public List<FlightBooking> getReservationdetails()
	{
		Session session = SessionUtil.getSession();
		Query query = session.createQuery("from FlightBooking");
		List<FlightBooking> FBD = query.list();
		session.close();
		return FBD;
	}
	
	public int cancelReservation(int id)
	{
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		String hql = "delete from FlightBooking where id = :id";
		Query query = session.createQuery(hql);
		query.setInteger("id", id);
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		tx.commit();
		session.close();
		return rowCount;
	}

}
