package com.simplilearn.flight.flyaway;
import java.util.List;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import com.simplilearn.flight.flyaway.entity.FlightBooking;
import com.simplilearn.flight.flyaway.entity.dao.FlightBookingDao;


@Path("/reservation")
public class FlightBookingResource
{
@GET
@Produces("application/json")
public List<FlightBooking> getFlight() {
	FlightBookingDao dao = new FlightBookingDao();
	List<FlightBooking> flights = dao.getReservationdetails();
	return flights;
}


@POST
@Consumes("application/json")
public Response addReservation(FlightBooking flight) {

	FlightBookingDao dao = new FlightBookingDao();
	dao.addReservation(flight);

	return Response.ok().build();
}


@DELETE
@Path("/{id}")
@Consumes("application/json")
public Response deleteFlight(@PathParam("id") int id) {
	FlightBookingDao dao = new FlightBookingDao();
	int count = dao.cancelReservation(id);
	if (count == 0) {
		return Response.status(Response.Status.BAD_REQUEST).build();
	}
	return Response.ok().build();
}
}